<?php
include_once('rootPath.php');
include_once('supportedFormats.php');

//Wenn eine größe sich an das Verhältnis anpassen soll, muss sie null gesetzt werden. Werden beide Angegeben wird eine passend überschrieben.
//Beide null -> originalgröße
function convertImageColorFiletype($inputPath,$colorProfile,$outputPath,$width = null,$height = null){

    $imageColorProfiles = getImageProfileArray();
    $inputPath = rootPath().$inputPath;
    $i = new Imagick();
    $i->readImage($inputPath);
    $colorSpace = $imageColorProfiles[$colorProfile];
    $i->transformImageColorspace($colorSpace);

    if($width == 'undefined'){$width = null;}
    if($height == 'undefined'){$height = null;}

    if($width != null and $height != null){

        $i->cropThumbnailImage($width,$height);
    }else if($width == null xor $height == null){
        if($width == null){$width = 0;}
        if($height == null){$height = 0;}
        $i->scaleImage($width,$height);
    }

    $i->writeImage(rootPath().$outputPath);
    $i->destroy();
}

//verfügbare Farbprofile können hier gelesen und hinzugefügt werden
//http://php.net/manual/de/imagick.constants.php#imagick.constants.colorspace
function getImageProfileArray(){
    return array(
        'sRGB' => imagick::COLORSPACE_SRGB,
        'graustufen' => imagick::COLORSPACE_GRAY,
        'RGB' => imagick::COLORSPACE_RGB,
        'CMYK' => imagick::COLORSPACE_CMYK,
        'HSB' => imagick::COLORSPACE_HSB
    );
}

//Gespeicherte Thumbnails sind immer jpg
function makeImageThumbnail($inputPath, $width = 100 , $height = 100){

    $realPath = rootPath().$inputPath;
    $pos = strrpos($inputPath,'.');
    $outputPath = rootPath().thumbnailPath().substr($inputPath,0,$pos).'.jpg';
    
    $i = new Imagick();
    $i->readImage($realPath);
    $i->transformImageColorspace(imagick::COLORSPACE_SRGB);
    $i->cropThumbnailImage($width,$height);
    $i->writeImage($outputPath);
    $i->destroy();
}

//benutzt ffmpeg. Sollte mit imagemagick installiert worden sein (war bei mir so)
function makeVideoThumbnail($inputPath,$width = 100 , $height = 100){
    $realPath = rootPath().$inputPath;
    $pos = strrpos($inputPath,'.');

    $outputPath = rootPath().thumbnailPath().substr($inputPath,0,$pos).'.jpg';

    $shellCommand = 'ffmpeg -ss 0.5 -i '.$realPath.' -t 1 -filter "scale=-1:'.$height.',crop='.$width.':'.$height.'" -f image2 '.$outputPath;
    shell_exec($shellCommand);
}

function convertVideoFiletype($inputPath,$outputPath){
    $realInputPath = rootPath().$inputPath;
    $realOutputPath = rootPath().$outputPath;
    $shellCommand = 'ffmpeg -i '.$realInputPath.' '.$realOutputPath;
    shell_exec($shellCommand);
}
function convertAudioFiletype($inputPath,$outputPath){
    $realInputPath = rootPath().$inputPath;
    $realOutputPath = rootPath().$outputPath;
    $shellCommand = 'ffmpeg -i '.$realInputPath.' '.$realOutputPath;
    shell_exec($shellCommand);
}

function getDataType($path){
    $path = rootPath().$path;
    $pos = strrpos($path,'.');
    return strtolower(substr($path,$pos + 1));
}

function makeThumbnail($inputPath,$width = 800 , $height = 540){
    if(getKategory(getDataType($inputPath)) == 'video'){
        makeVideoThumbnail($inputPath,$width,$height);
    }else if(getKategory(getDataType($inputPath)) == 'picture'){
        makeImageThumbnail($inputPath,$width,$height);
    }
}

function getThumbnailPath($mediaPath){
    
    if(getKategory(getDataType($mediaPath)) == 'audio'){
        return getAudioThumbnailPath();
    }
    $realPath = $mediaPath;
    $pos = strrpos($mediaPath,'.');
    return thumbnailPath().substr($mediaPath,0,$pos).'.jpg';

}

function getAudioThumbnailPath(){
    return 'thumbnails/media/audioThumbnail.svg';
}


/*//Example:

//Pfad für das Ausgangsbild
$picPath = 'bilder/Flower-ICC.jpg';


//Pfad für das entstehende Bild - Endung bestimmt Dateiformat
$outPath = 'bilderOUT/Flower.png';


//los gehts:
convertImageColorFiletype($picPath,'grau',$outPath,200,50);
makeThumbnail($picPath);
*/

//convertAudio('bestFriendSofiTukker.mp3','out.wav');

//convertVideoFiletype('media/videos/bigBunny.mkv','media/temp/bigBunny.mp4');

//makeThumbnail('bilder/1518598597sample.png');

