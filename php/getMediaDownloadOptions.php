<?php
include_once("rootPath.php");
include_once("supportedFormats.php");
include_once("convertFiles.php");


function getMediaOptions($inputPath){
    $realPath = rootPath().$inputPath;
    $dataType = getDataType($inputPath);

    $returnArray = array(
        "formats" => getFormatsByFileType($dataType)
    );

    if(getKategory($dataType) == "picture"){
        $returnArray["colorSchemes"] = array_keys(getImageProfileArray());
        $returnArray["resolutionPicker"] = true;
    }else{
        $returnArray["resolutionPicker"] = false;
    }
    $returnArray["kategory"] = getKategory($dataType);
    return $returnArray;
}




