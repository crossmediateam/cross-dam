<?php



//gibt ICC zurück, falls nicht vorhanden null
function getICC($picturePath){
    $picturePath = realpath(dirname(__FILE__)).'/../'.$picturePath;
    $image = new imagick($picturePath);
    try {
        $existingICC = $image->getImageProfile('icc');
    } catch (ImagickException $e) {
        $existingICC = null;
    }
    return $existingICC;
}

function getPDFMeta($pdfPath){
    $rootPath = realpath(dirname(__FILE__)).'/../';
    $pdfPath = $rootPath.$pdfPath;
    $parser = \..\php-libraries\Smalot\PdfParser\Parser();
    $pdf    = $parser->parseFile($pdfPath);
    $text   = $pdf->getDetails();
    return $text;
}

/*//Example
print_r(getICC('bilder/Flower-ICC.jpg'));
*/

print_r(getPDFMeta(realpath(dirname(__FILE__)).'/../'.'bilderOUT/Flower.pdf'));