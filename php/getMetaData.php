<?php
include_once('rootPath.php');

function getPDFData($path){
    $outArray = array();
    $realPath = $path;
    $image = new Imagick();
    $image->readImage($realPath);
    $count = $image->getNumberImages();
    $outArray['pageCount'] = $count;

    $fileSize = filesize($realPath);
    $outArray['size'] = formatBytes($fileSize);
    return $outArray;
}



//$vid = rootPath()."media/videos/bigBunny.mp4";
//echo $vid;
// print_r(getVideoData($vid));



function getVideoData($video) {
    $video = $video;
    //$realPath = rootPath().$video;
    $ffmpeg = 'ffmpeg';
    $command = $ffmpeg . ' -i ' . $video . ' -vstats 2>&1';
    $output = shell_exec($command);

    $regex_sizes = "/Video: ([^,]*), ([^,]*), ([0-9]{1,4})x([0-9]{1,4})/"; // or : $regex_sizes = "/Video: ([^\r\n]*), ([^,]*), ([0-9]{1,4})x([0-9]{1,4})/"; (code from @1owk3y)
    if (preg_match($regex_sizes, $output, $regs)) {
        $codec = $regs [1] ? $regs [1] : null;
        $width = $regs [3] ? $regs [3] : null;
        $height = $regs [4] ? $regs [4] : null;
    }

    $regex_duration = "/Duration: ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}).([0-9]{1,2})/";
    if (preg_match($regex_duration, $output, $regs)) {
        $hours = $regs [1] ? $regs [1] : null;
        $mins = $regs [2] ? $regs [2] : null;
        $secs = $regs [3] ? $regs [3] : null;
        $ms = $regs [4] ? $regs [4] : null;
    }

    $time = $hours. ':' .$mins. ':'
    . $secs;

    $fileSize = filesize($video);

    return array('codec' => $codec,
        'width' => $width,
        'height' => $height,
        'duration' => $time,
        'size' => formatBytes($fileSize)
    );
}


function formatBytes($bytes, $precision = 2) {
    $units = array('B', 'KB', 'MB', 'GB', 'TB');

    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);

    // Uncomment one of the following alternatives
    $bytes /= pow(1024, $pow);
    // $bytes /= (1 << (10 * $pow));

    return round($bytes, $precision) . ' ' . $units[$pow];
}

//$path = rootPath().'media/audio/bestFriendsSofiTukker.mp3';
