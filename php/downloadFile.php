<?php
include_once("rootPath.php");
include_once("convertFiles.php");
/*
Benötigte GET-Parameter:
    path: Pfad zum Medium (Beispiel: 'media/videos/bigBunny.mkv);
    kategory: Kategorie ('picture','video' oder 'audio')
    outputFormat: Format des Outputs (zum Beispiel "mp4")

Benötigt bei Bild/PDF (kategory "picture")
    resX: breite in Pixeln
    resY: höhe in Pixeln
    colorScheme: Farbprofil ("sRGB")

*/


$inputPath = $_GET['path'];
$kategory = $_GET['kategory'];
$outputFormat = $_GET["outputFormat"];

$outputPath = "media/temp/".uniqid().".".$outputFormat;

if($kategory == "picture"){

    if(isset($_GET["resX"])){
        $width = $_GET["resX"];
    }else{
        $width = null;
    }

    if(isset($_GET["resY"])){
        $height = $_GET["resY"];
    }else{
        $height = null;
    }
    
    $colorProfile = $_GET["colorScheme"];
    

    convertImageColorFiletype($inputPath,$colorProfile,$outputPath,$width,$height);
}else if($kategory == "video"){

    convertVideoFiletype($inputPath,$outputPath);
}else if($kategory == "audio"){

    convertAudioFiletype($inputPath,$outputPath);
}

echo str_replace ( rootPath() , '' , $outputPath);
//$file = $outputPath;

/*
    if (file_exists($file)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($file).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);
        exit;
}

*/
