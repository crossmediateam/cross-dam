<?php
//$picturePath = 'media/bilder/Flower-ICC.jpg';


include_once('convertFiles.php');
include_once('getMediaDownloadOptions.php');

$thumbnailPath = getThumbnailPath($picturePath);

$mediaOptions = getMediaOptions($picturePath);
function formatOptions($array){
    $outputString = '';
    foreach ($array as $option) {
        $outputString .= '<option>'.$option.'</option>';
    }
    return $outputString;
}

$kategory = getKategory(getDataType($picturePath));
echo '<p id="mediaKategory" style="visibility: hidden;">'.$kategory.'</p>';
echo '<p id="mediaPath" style="visibility: hidden;">'.$picturePath.'</p>';
?>

<script>

/*
$('#widthCheck').click(function() {
});

$('#heightCheck').click(function() {
    //$('#inputheight').attr('disabled',! this.checked)
});*/

var globalAlreadyDownloadedBool = false;


function onDownloadButtonClicked(){
    $('#mainButton').text('processing...');
    globalAlreadyDownloadedBool = false;
    var kategory = $('#mediaKategory').text();
    var picPath = $('#mediaPath').text();
    var fileFormat = $('#exampleFormControlSelect1').find(":selected").text();
    if(kategory == 'picture'){
        var colorScheme = $('#exampleFormControlSelect2').find(":selected").text();
        var resX = $('#inputwidth').val();
        var resY = $('#inputheight').val();
        var link = 'php/downloadFile.php?path='+picPath+'&kategory='+kategory+'&outputFormat='+fileFormat+'&resX='+resX+'&resY='+resY+'&colorScheme='+colorScheme;
        //var link = 'php/downloadFile.php?path='+picPath+'&kategory='+kategory+'&outputFormat='+fileFormat+;
    }else{
        var link = 'php/downloadFile.php?path='+picPath+'&kategory='+kategory+'&outputFormat='+fileFormat;
    }
    //window.location = link;
    //alert(kategory);
    var path = ajaxRequest(link,OnLinkGotten);

}


function ajaxRequest(link,responseFunction){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
                responseFunction(this.responseText);
            }
        };
        xmlhttp.open("GET", link, true);
        xmlhttp.send();
}

var globalCheckLink ="";
function OnLinkGotten(checkLink){
    globalCheckLink = checkLink;
    console.log(globalCheckLink);
    window.setInterval(function(){
        ajaxRequest("php/fileExists.php?path=" + checkLink,OnStatusGotten);
    }, 200);
}

function OnStatusGotten(statusBool){
    if(statusBool == 'false'){

    }else if(statusBool == 'true'){
        if(!globalAlreadyDownloadedBool){
            window.location = 'php/downloadOriginalFile.php?path='+globalCheckLink;
            globalAlreadyDownloadedBool = true;
            $('#mainButton').text('Done');
        }
    }
}

function onWidthToggle() {
    // check if checkbox is checked
    if (document.querySelector('#widthCheck').checked) {
      // if checked
        $('#inputwidth').attr('disabled',true);
        $('#inputwidth').val('');
      
    } else {
      // if unchecked
      $('#inputwidth').attr('disabled',false)
    }
  }

  function onHeightToggle() {
    // check if checkbox is checked
    if (document.querySelector('#heightCheck').checked) {
      // if checked
        $('#inputheight').attr('disabled',true)
        $('#inputheight').val('');
      
    } else {
      // if unchecked
      $('#inputheight').attr('disabled',false)
    }
  }






/*
Benötigte GET-Parameter:
    path: Pfad zum Medium (Beispiel: 'media/videos/bigBunny.mkv);
    kategory: Kategorie ('picture','video' oder 'audio')
    outputFormat: Format des Outputs (zum Beispiel "mp4")

Benötigt bei Bild/PDF (kategory "picture")
    resX: breite in Pixeln
    resY: höhe in Pixeln
    colorScheme: Farbprofil ("sRGB")
*/
</script>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                <h1 class="h2">Herunterladen</h1>
            </div>

            <h4>Wählen Sie hier ihre Downloadeinstellungen</h4>

            <div class="row align-items-center">
                <div class="col-md-2">
                    <img class="img-fluid" src="<?php echo $thumbnailPath;?>">
                </div>
                <div class="col-md-10">
                    <p>Einstellungen für <?php echo basename($picturePath); ?></p>
                    <a href="/cross-dam/php/downloadOriginalFile.php?path=<?php echo $picturePath;?>"><button type="button" class="btn btn-primary btn-sm" style="background-color: rgb(0, 181, 148);border-color: rgb(0, 181, 148);">Original herunterladen</button></a>
                </div>
            </div>
            <div class="row top-buffer">
                <div class="col-md-2">
                    <h5>Format anpassen</h5>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-2">
                    <p>Dateiformat</p>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <select class="form-control" id="exampleFormControlSelect1" style="width:100px;">
<?php
 echo formatOptions($mediaOptions["formats"]);
?>
                        </select>
                    </div>
                </div>
            </div>
<?php if($mediaOptions["kategory"] == 'picture'){?>
            <div class="row align-items-center">
                <div class="col-md-2">
                    <p>Farbschema</p>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <select class="form-control" id="exampleFormControlSelect2" style="width:100px;">
<?php 
 echo formatOptions($mediaOptions["colorSchemes"]);
 ?>
                        </select>
                    </div>
                </div>
            </div>
<?php }
if($mediaOptions["resolutionPicker"]){?>
            <div class="row align-items-center">
                <div class="col-md-12">
                    <p><b>Auflösung</b></p>
                </div>
                <div class="col-md-6" style="padding-bottom:20px;">
                    <form class="form-inline">
                        <div class="form-group">
                            <label style="width:37px;" for="inputwidth">Breite</label>
                            <input type="number" id="inputwidth" class="form-control mx-sm-3">
                        </div>
                    </form>
                </div>
                <div class="col-md-6" style="padding-bottom:20px;">
                    <div class="form-row">
                        <input type="checkbox" class="form-check-input" id="widthCheck" onclick="onWidthToggle()">
                        <label class="form-check-label" for="exampleCheck1">Proportionen beibehalten</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <form class="form-inline">
                        <div class="form-group">
                            <label style="width:37px;" for="inputheight">Höhe</label>
                            <input type="number" id="inputheight" class="form-control mx-sm-3">
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <div class="form-row">
                        <input type="checkbox" class="form-check-input" id="heightCheck" onclick="onHeightToggle()">
                        <label class="form-check-label" for="exampleCheck1">Proportionen beibehalten</label>
                    </div>
                </div>
            </div>
<?php } ?>
            <div class="row top-buffer offset-md-2">
                <div class="col-md">
                    <button style="margin-bottom: 50px;margin-top:30px; background-color: rgb(0, 181, 148);border-color: rgb(0, 181, 148);" onclick="onDownloadButtonClicked()" type="button" id="mainButton" class="btn btn-primary btn-sm">Herunterladen</button>
                </div>
            </div>


        </main>
<?php

