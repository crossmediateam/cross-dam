<?php

function getAttrDictionary(){
    return array(
        'data_size' => 'Größe:',
        'creation_date' => 'Datei hochgeladen:',
        'model' => 'Kamera:',
        'aperture' => 'Blende:',
        'shutterspeed' => 'Belichtungszeit:',
        'iso' => 'ISO:' 
    );
}

function generateList($dataArray){
    $gen = new detailsGenerator();

    foreach (getAttrDictionary() as $key => $value){
        if($dataArray[$key] != null && $dataArray[$key] != '-' && $dataArray[$key] != 'nicht bekannt'){
            $gen->addPoint($value, $dataArray[$key]);
        }
    }

    return $gen->outputString;
}

class detailsGenerator{ 
    public $numberAddedPoints = 0;
    public $outputString;
    
    
    function addPoint($detailKey, $detailValue) { 
        if(($this->numberAddedPoints % 3 == 0) && $this->numberAddedPoints != 0){
            $this->addLine();
        }
        if($this->numberAddedPoints % 3 == 0){
            $this->startRow();
        }
        $this->numberAddedPoints++;

        $this->outputString.= '<div class="col-md-2"><p>'.$detailKey.'</p></div>
        <div class="col-md-2"><p>'.$detailValue.'</p></div>';

        if($this->numberAddedPoints % 3 == 0){
            $this->endRow();
        }
    } 

    function endRow(){
        $this->outputString.= '</div>';
    }

    function startRow(){
        $this->outputString.='<div class="row">';
    }

    function addLine(){
        $this->outputString.='<hr style="margin-bottom: 20px; margin-top: 5px">';
    }
} 

?>
<!--
<div class="row">
                        <div class="col-md-2">
                            <p>Größe:</p>
                        </div>
                        <div class="col-md-2">
                            <p>3,5 MB</p>
                        </div>


                        <div class="col-md-2">
                            <p>Datei hochgeladen:</p>
                        </div>
                        <div class="col-md-2">
                            <p>13.02.2018</p>
                        </div>
                        <div class="col-md-2">
                            <p>Kamera:</p>
                        </div>
                        <div class="col-md-2">
                            <p>Canon EOS 100D</p>
                        </div>
                    </div>
                    <hr style="margin-bottom: 20px; margin-top: 5px">


                
                
                
                
                -->