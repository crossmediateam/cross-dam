<?php

function getSupportedFormats(){
    return array(
        "jpg" => array("kategory" => "picture"),
        "jpeg" => array("kategory" => "picture"),
        "png" => array("kategory" => "picture"),
        "gif" => array("kategory" => "picture"),
        "psd" => array("kategory" => "picture"),
        "svg" => array("kategory" => "picture"),
        "tiff" => array("kategory" => "picture"),
        "pdf" => array("kategory" => "picture"),
        "pdf" => array("kategory" => "picture"),
        "mp4" => array("kategory" => "video"),
        "mkv" => array("kategory" => "video"),
        "mov" => array("kategory" => "video"),
        "avi" => array("kategory" => "video"),
        "mp3" => array("kategory" => "audio"),
        "wav" => array("kategory" => "audio")
    );
}


function getFormatsByKategory($kategory){
    $kategories = array(
        "picture" => array("jpg","png","gif","psd","svg","tiff","pdf","pdfx"),
        "video" => array("mp4","mkv","mov","avi"),
        "audio" => array("mp3","wav")
    );
    return $kategories[$kategory];
}

function getKategory($format){
    return getSupportedFormats()[$format]["kategory"];
}

function getFormatsByFileType($fileType){
    return getFormatsByKategory(getKategory($fileType));
}