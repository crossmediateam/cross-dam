<?php
session_start();
if(!isset($_SESSION['userid']))
{
        header("location: login");
}
#LOGOUT
if(isset($_GET['logout'])) {

    session_destroy();
    header('Location: login');
}
require_once("db_connect.php");
require_once("php/convertFiles.php");



#SPEICHERT BILD AUF WEBSPACE

if(isset($_POST["send"]) && $_POST["send"] == "1") {

$target_Path = "bilder/";
$filename = round(microtime(true)).$_FILES["bild_datei"]["name"];

if(isset($_POST["artist"])){
$artist = $_POST["artist"];}
else{$artist = "-";}

if(isset($_POST["date"])){
  $date = $_POST["date"];}
  else{$date = "-";}

if(isset($_POST["width"])){
  $width = $_POST["width"];}
  else{$width = "-";}

if(isset($_POST["high"])){
  $high = $_POST["high"];}
  else{$high = "-";}

if(isset($_POST["size"])){
  $size = $_POST["size"];}
  else{$size = "-";}

if(isset($_POST["icc"])){
  $icc = $_POST["icc"];}
  else{$icc = "-";}

if(isset($_POST["marke"])){
  $marke = $_POST["marke"];}
  else{$marke = "-";}

if(isset($_POST["modell"])){
  $modell = $_POST["modell"];}
  else{$modell = "-";}

if(isset($_POST["shutter"])){
  $shutter = $_POST["shutter"];}
  else{$shutter = "-";}

if(isset($_POST["aperture"])){
  $aperture = $_POST["aperture"];}
  else{$aperture = "-";}

if(isset($_POST["iso"])){
  $iso = $_POST["iso"];}
  else{$iso = "-";}

  if(isset($_POST["color_type"])){
    $color_type = $_POST["color_type"];}
    else{$color_type = "-";}

    if(isset($_POST["bit"])){
      $bits = $_POST["bit"];}
      else{$bits = "-";}

  if(isset($_POST["format"])){
    $format = $_POST["format"];}
    else{$format = "-";}



$target_Path = $target_Path . $filename;
move_uploaded_file( $_FILES['bild_datei']['tmp_name'], $target_Path );
makeThumbnail($target_Path);

#SPEICHERT BILDER IN DB
$sql ="INSERT INTO bildergalerie (bildurl,data_size,width,height,icc,creation_date,artist,make,model,aperture,shutterspeed,iso,color_type,filetype,bit_dep) VALUES ('$filename','$size','$width','$high','$icc','$date','$artist','$marke','$modell','$aperture','$shutter','$iso','$color_type','$format','$bits')";
$succes=$pdo->exec($sql);

}


?>

<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<!-- Custom styles for this template -->
<link href="css/dashboard.css" rel="stylesheet">
<link href="css/add_dashboard.css" rel="stylesheet">

<title>Cross-DAM</title>

<link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
<link rel="manifest" href="site.webmanifest">
<link rel="mask-icon" href="safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">

</head>

<body>
  <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="main.php"><img style="height: auto;width: 70px;margin-left: 15px" src="img/ui/logo.svg"></a>
    <input class="form-control form-control-dark w-100" type="text" style="background-color:#495057;" disabled="true">
    <ul class="navbar-nav px-3 navbar-upload">
      <li class="nav-item text-nowrap">
        <a class="nav-link" href="upload.php"><span data-feather="upload-cloud"></span> Hochladen</a>
      </li>
    </ul>
    <ul class="navbar-nav px-3 navbar-upload">
      <li class="nav-item text-nowrap">
        <a class="nav-link" href="?logout=1"><span data-feather="log-out"></span> Abmelden</a>
      </li>
    </ul>
  </nav>

  <div class="container-fluid">
    <div class="row">
      <nav class="col-md-2 d-none d-md-block bg-light sidebar">
        <div class="sidebar-sticky">
          <ul class="nav flex-column">
            <li>
              <div id="accordion">
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <span data-feather="database"></span>
                                        Meine Daten
                                    </button>
                                </h5>
                  </div>

                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="nav-item">
                      <a class="nav-link" href="#">
                        <span data-feather="film"></span>
                        Video
                    </a>
                    </div>
                    <div class="nav-item">
                      <a class="nav-link" href="#">
                        <span data-feather="image"></span>
                        Foto
                    </a>
                    </div>
                    <div class="nav-item">
                      <a class="nav-link" href="#">
                        <span data-feather="mic"></span>
                        Audio
                    </a>
                    </div>
                    <div class="nav-item">
                      <a class="nav-link" href="#">
                        <span data-feather="file"></span>
                        PDF
                    </a>
                    </div>
                  </div>
                </div>

              </div>
          </ul>

        </div>
      </nav>
    </div>
    <div class="container-fluid">


      <div class="row">

        <div class="col-10 offset-1">
          <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
              <h1 class="h2">Hochladen</h1>
            </div>

            <h4>Dateien hochladen</h4>

              <div class="row">
                  <div class="col-md-4"></div>
                  <div class="col-md-4">
                      <div class="form-group">
                          <form id="bild_form" class="center upload text-center" enctype="multipart/form-data" action="?speichern=1&upload=1" method="POST" onsubmit="return checkform(this)">
                              <input type="hidden" name="MAX_FILE_SIZE" value="300000000" />
                              <input type="hidden" name="send" value="1" />
                              <input id="image-upload" class="btn center" name="bild_datei" type="file" id="uploadbild" />
                      </div>
                  </div>
                  <div class="col-md-4"></div>
              </div>
              <div class="row">
                  <div class="col-md-4"></div>
                  <div class="col-md-4">
                      <div class="center" id="image-preview"></div>
                  </div>
                  <div class="col-md-4"></div>
              </div>

              <div id="jpg_meta" class="meta_container">
                <input type="hidden" class="form-control" name="format" value="jpg">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Fotograf</label>
                            <input type="text" class="form-control" name="artist" id="input_artist">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Aufnahmedatum</label>
                            <input type="text" class="form-control" name="date" id="input_date">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Dateigröße</label>
                            <input type="text" class="form-control" name="size" id="input_size">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Weite</label>
                            <input type="text" class="form-control" name="width" id="input_width">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Höhe</label>
                            <input type="text" class="form-control" name="high" id="input_high">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Frabprofil</label>
                            <input type="text" class="form-control" name="icc" id="input_icc">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group top-buffer">
                            <label for="formGroupExampleInput2">Verschlusszeit</label>
                            <input type="text" class="form-control" name="shutter" id="input_shutter">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Blende</label>
                            <input type="text" class="form-control" name="aperture" id="input_aperture">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">ISO</label>
                            <input type="text" class="form-control" name="iso" id="input_iso">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group top-buffer">
                            <label for="formGroupExampleInput2">Marke</label>
                            <input type="text" class="form-control" name="marke" id="input_marke">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Modell</label>
                            <input type="text" class="form-control" name="modell" id="input_modell">
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                </div>
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                     <input style="margin-left: auto; margin-right: auto; display: block; margin-bottom: 15px;background-color: rgb(0, 181, 148);border-color: rgb(0, 181, 148);" class="btn btn-primary" type="submit" value="hochladen" />
                    </div>
                    <div class="col-md-4"></div>
                </div>
              </div>

              <div id="png_meta" class="meta_container">
                <input type="hidden" class="form-control" name="format" value="png">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Urheber</label>
                            <input type="text" class="form-control" name="type" id="png_artist" value="nicht bekannt">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Aufnahmedatum</label>
                            <input type="text" class="form-control" name="date" id="png_date">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Dateigröße</label>
                            <input type="text" class="form-control" name="size" id="png_size">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Weite</label>
                            <input type="text" class="form-control" name="width" id="png_width">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Höhe</label>
                            <input type="text" class="form-control" name="high" id="png_high">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Frabprofil</label>
                            <input type="text" class="form-control" name="icc" id="png_icc">
                        </div>
                    </div>
                </div>
                  <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="formGroupExampleInput2">Farbtyp</label>
                        <input type="text" class="form-control" name="color_type" id="png_color">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="formGroupExampleInput2">Bit Tiefe</label>
                        <input type="text" class="form-control" name="bit" id="png_bit">
                    </div>
                </div>
                </div>


                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <span class="center-block">
                     <input  class="btn btn-primary" type="submit" value="hochladen" />
                   </span>
                    </div>
                    <div class="col-md-4"></div>
                </div>
              </div>



              <div id="video_meta" class="meta_container">
                <input id="video_format" type="hidden" class="form-control" name="format" value="">

                <div class="row">
                    <!-- <div class="col-md-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Urheber</label>
                            <input type="text" class="form-control" name="artist" id="video_artist" value="nicht bekannt">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Codec</label>
                            <input type="text" class="form-control" name="codec" id="codec_date">
                        </div>
                    </div> -->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Dateigröße</label>
                            <input type="text" class="form-control" name="size" id="video_size">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- <div class="col-md-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Weite</label>
                            <input type="text" class="form-control" name="width" id="video_width">
                        </div>
                    </div> -->
                    <!-- <div class="col-md-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Höhe</label>
                            <input type="text" class="form-control" name="high" id="video_high">
                        </div>
                    </div> -->
                    <!-- <div class="col-md-4">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Länge</label>
                            <input type="text" class="form-control" name="duration" id="video_duration">
                        </div>
                    </div> -->
                  </div>
                  <div class="row">
                      <div class="col-md-4"></div>
                      <div class="col-md-4">
                          <span class="center-block">
                       <input  class="btn btn-primary" type="submit" value="hochladen" />
                     </span>
                      </div>
                      <div class="col-md-4"></div>
                  </div>
                </div>


                <div id="pdf_meta" class="meta_container">
                  <input type="hidden" class="form-control" name="format" value="pdf">

                  <div class="row">
                      <!-- <div class="col-md-4">
                          <div class="form-group">
                              <label for="formGroupExampleInput2">Urheber</label>
                              <input type="text" class="form-control" name="artist" id="pdf_artist" value="nicht bekannt">
                          </div>
                      </div> -->
                      <div class="col-md-4">
                          <div class="form-group">
                              <label for="formGroupExampleInput2">Dateigröße</label>
                              <input type="text" class="form-control" name="size" id="pdf_size">
                          </div>
                      </div>
                      <!-- <div class="col-md-4">
                          <div class="form-group">
                              <label for="formGroupExampleInput2">Seiten</label>
                              <input type="text" class="form-control" name="pages" id="pdf_pages">
                          </div>
                      </div> -->
                    </div>
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <span class="center-block">
                         <input  class="btn btn-primary" type="submit" value="hochladen" />
                       </span>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                  </div>

                  <div id="mp3_meta" class="meta_container">
                    <input type="hidden" class="form-control" name="format" value="mp3">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput2">Dateigröße</label>
                                <input type="text" class="form-control" name="size" id="mp3_size">
                            </div>
                        </div>
                      </div>
                      <div class="row">
                          <div class="col-md-4"></div>
                          <div class="col-md-4">
                              <span class="center-block">
                           <input  class="btn btn-primary" type="submit" value="hochladen" />
                         </span>
                          </div>
                          <div class="col-md-4"></div>
                      </div>
                    </div>






              </div>


            </div>
            <div class="row top-buffer">
              <div class="col-md-4">

              </div>
              <div class="col-md-4">

              </div>
              <div class="col-md-4">


              </div>
            </div>
          </main>
        </div>
      </div>
    </div>
  </div>
  </div>
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script type="text/javascript" src="js/jquery.uploadPreview.min.js"></script>
  <script type="text/javascript" src="js/fill_input.js"></script>
  <script src="js/card_overlay.js"></script>
  <script src="js/main_ajax.js"></script>
  <script src="js/upload.js"></script>
  <!-- Icons -->
  <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
  <script>
    feather.replace();
  </script>
</body>

</html>
