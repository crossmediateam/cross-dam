

function check_visible(){
  $(".meta_container").each(function(){
    if($(this).css("display") == "none"){
      $(this).remove();
    }
  });
}



function fill_input(json) {
  // fill in date for jpg

  if(json["type"] == "jpg")
  {
    $("#jpg_meta").css("display","block");
    check_visible();
  if (json["date:create"]) {
    var res = json["date:create"].slice(0, 10);
    $("#input_date").val(res);
  } else {
    $("#input_date").val("nicht bekannt");
  }

  // fill in artist
  if (json["exif:Artist"]) {
    $("#input_artist").val(json["exif:Artist"]);
  } else {
    $("#input_artist").val("nicht bekannt");
  }

  // fill in size
  if (json["size"]) {
    $("#input_size").val(json["size"]);
  } else {
    $("#input_size").val("nicht bekannt");
  }

  // fill in length
  if (json["exif:ExifImageLength"]) {
    $("#input_high").val(json["exif:ExifImageLength"] + " px");
  } else {
    $("#input_high").val("nicht bekannt");
  }

  // fill in width
  if (json["exif:ExifImageWidth"]) {
    $("#input_width").val(json["exif:ExifImageWidth"] + " px");
  } else {
    $("#input_width").val("nicht bekannt");
  }

  // fill in icc
  if (json["icc:description"]) {
    $("#input_icc").val(json["icc:description"]);
  }
  else if(json["exif:ColorSpace"] == "1"){
    $("#input_icc").val("sRGB");
  }
  else if(json["exif:ColorSpace"] == "2"){
    $("#input_icc").val("Adobe RGB");
  }
  else {
    $("#input_icc").val("nicht bekannt");
  }

  // fill in make
  if (json["exif:Make"]) {
    $("#input_marke").val(json["exif:Make"]);
  } else {
    $("#input_marke").val("nicht bekannt");
  }

  // fill in model
  if (json["exif:Model"]) {
    $("#input_modell").val(json["exif:Model"]);
  } else {
    $("#input_modell").val("nicht bekannt");
  }

  // fill in shutterspeed
  if (json["exif:ExposureTime"]) {
    $("#input_shutter").val(json["exif:ExposureTime"]);
  } else {
    $("#input_shutter").val("nicht bekannt");
  }

  // fill in aperture
  if (json["exif:FNumber"]) {
    $("#input_aperture").val(json["exif:FNumber"]);
  } else {
    $("#input_aperture").val("nicht bekannt");
  }

  // fill in iso
  if (json["exif:ISOSpeedRatings"]) {
    $("#input_iso").val(json["exif:ISOSpeedRatings"]);
  } else {
    $("#input_iso").val("nicht bekannt");
  }
}

// =================================================

if(json["type"] == "png")
{
  $("#png_meta").css("display","block");
  check_visible();





if (json["date:create"]) {
  var res = json["date:create"].slice(0, 10);
  $("#png_date").val(res);
} else {
  $("#png_date").val("nicht bekannt");
}

// fill in Color Type
  if(json["png:IHDR.color-type-orig"] == "6"){
    $("#png_color").val("RGBA");
  }
  else if (json["png:IHDR.color-type-orig"] == "1") {
    $("#png_color").val("Pallet");
  }
  else if (json["png:IHDR.color-type-orig"] == "2") {
    $("#png_color").val("Color");
  }
  else if (json["png:IHDR.color-type-orig"] == "4") {
    $("#png_color").val("Alpha Channel");
  }
 else {
  $("#png_color").val("nicht bekannt");
}

// fill in size
if (json["size"]) {
  $("#png_size").val(json["size"]);
} else {
  $("#png_size").val("nicht bekannt");
}

// fill in length,width
if (json["png:IHDR.width,height"]) {
  var width = json["png:IHDR.width,height"].split(",");
  $("#png_width").val(width[0] + " px");
  $("#png_high").val(width[1] + " px");
} else {
  $("#png_high").val("nicht bekannt");
  $("#png_width").val("nicht bekannt");
}

// fill in icc
if (json["png:sRGB"]) {
  $("#png_icc").val("sRGB");
}
else {
  $("#input_icc").val("nicht bekannt");
}


if (json["png:IHDR.bit_depth"]) {
  $("#png_bit").val(json["png:IHDR.bit_depth"]);
}
else {
  $("#png_bit").val("nicht bekannt");
}




}

// ===================================================


if(json["type"] == "mov" || json["type"] == "mp4" || json["type"] == "mkv" || json["type"] == "avi")
{
  $("#video_meta").css("display","block");
  check_visible();
$("#video_format").val(json["type"]);


// fill in size

$('#image-preview').html("<img src='"+json["thumb"]+"'>");

if (json["size"]) {
  $("#video_size").val(json["size"]);
} else {
  $("#png_size").val("nicht bekannt");
}
}


// ================================

if(json["type"] == "pdf")
{
  $('#image-preview').html("<img src='"+json["thumb"]+"'>");

  $("#pdf_meta").css("display","block");
  check_visible();
// fill in size
if (json["size"]) {
  $("#pdf_size").val(json["size"]);
} else {
  $("#pdf_size").val("nicht bekannt");
}
}

// =========================================

if(json["type"] == "mp3")
{
  $('#image-preview').html("<img src='"+json["thumb"]+"'>");

  $("#mp3_meta").css("display","block");
  check_visible();
// fill in size
if (json["size"]) {
  $("#mp3_size").val(json["size"]);
} else {
  $("#mp3_size").val("nicht bekannt");
}
}




}
