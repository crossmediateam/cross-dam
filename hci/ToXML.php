<?php

function array_to_xml( $data, &$xml_data ) {
    foreach( $data as $key => $value ) {
        if( is_numeric($key) ){
            $key = 'item'.$key; //dealing with <0/>..<n/> issues
        }
        if( is_array($value) ) {
            $subnode = $xml_data->addChild($key);
            array_to_xml($value, $subnode);
        } else {
            $xml_data->addChild("$key",htmlspecialchars("$value"));
        }
     }
}

function saveAsXML($data, $fileName){
    $xml_data = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><data></data>');
    array_to_xml($data,$xml_data);
    $result = $xml_data->asXML($fileName);
    $file = file_get_contents($fileName);
    $replaced_string = str_replace("&#13;", "", $file);
    file_put_contents($fileName,$replaced_string);
}

?>
