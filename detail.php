<!-- Required meta tags -->

<?php
error_reporting(0);

session_start();
if(!isset($_SESSION['userid']))
{
        header("location: login");
}
#LOGOUT
if(isset($_GET['logout'])) {

    session_destroy();
    header('Location: login');
}

    include_once('db_connect.php');
    include_once('php/convertFiles.php');
    include_once('php/detailFunctions.php');

    $id = $_GET['id'];
    //$id = 184;
    if(is_numeric($id)){
        $sql = "SELECT * FROM bildergalerie WHERE ID=".$id.";";
        //print_r($pdo->query($sql));
        foreach ($pdo->query($sql) as $row){
            $mediaData = $row;
        }
        $mediaPath = $mediaData["bildurl"];
        //print_r($mediaData);

    }

    



?>


<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<!-- Custom styles for this template -->
<link href="css/dashboard.css" rel="stylesheet">
<link href="css/add_dashboard.css" rel="stylesheet">

<title>Details</title>

<link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
<link rel="manifest" href="site.webmanifest">
<link rel="mask-icon" href="safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">

</head>

<body>
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="main.php"><img style="height: auto;width: 70px;margin-left: 15px" src="img/ui/logo.svg"></a>
    <input class="form-control form-control-dark w-100" type="text" style="background-color:#495057;" disabled="true">
    <ul class="navbar-nav px-3 navbar-upload">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="upload.php"><span data-feather="upload-cloud"></span> Hochladen</a>
        </li>
    </ul>
    <ul class="navbar-nav px-3 navbar-upload">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="?logout=1"><span data-feather="log-out"></span> Abmelden</a>
        </li>
    </ul>
</nav>

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                    <li>
                        <div id="accordion">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <span data-feather="database"></span>
                                            Meine Daten
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="nav-item">
                                        <a class="nav-link" href="#">
                                            <span data-feather="film"></span>
                                            Video
                                        </a>
                                    </div>
                                    <div class="nav-item">
                                        <a class="nav-link" href="#">
                                            <span data-feather="image"></span>
                                            Foto
                                        </a>
                                    </div>
                                    <div class="nav-item">
                                        <a class="nav-link" href="#">
                                            <span data-feather="mic"></span>
                                            Audio
                                        </a>
                                    </div>
                                    <div class="nav-item">
                                        <a class="nav-link" href="#">
                                            <span data-feather="file"></span>
                                            PDF
                                        </a>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                </ul>

            </div>
        </nav>
    </div>
    <div class="container-fluid">


        <div class="row">

            <div class="col-10 offset-1">
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4" style="margin-bottom: 50px;">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                        <h1 class="h2">Details</h1>
                    </div>

                    <div class="row">
                        <div class="col-md-10">
                            <h4><?php echo $mediaPath?></h4>
                        </div>
                        <div class="col-md-2">
                            <a href="download.php?path=bilder/<?php echo $mediaPath; ?>"><input style="margin-left: auto; margin-right: auto; display: block; margin-bottom: 10px; background-color: rgb(0, 181, 148);border-color: rgb(0, 181, 148);" class="btn btn-primary" type="submit" value="Herunterladen" /></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="center" id="image-preview" style="height:250px;width:350px; margin-left:auto;margin-right:auto;">
                                <img style="height:250px;width:350px;" src="<?php echo getThumbnailPath('bilder/'.$mediaPath);?>"></img>
                            </div>
                        </div>
                    </div>
                    <?php echo generateList($mediaData);?>
                </main>
        </div>
    </div>
</div>
</div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript" src="js/jquery.uploadPreview.min.js"></script>
<script type="text/javascript" src="js/fill_input.js"></script>
<script src="js/card_overlay.js"></script>
<script src="js/main_ajax.js"></script>
<script src="js/upload.js"></script>
<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace();
</script>
</body>

</html>
