<!doctype html>
<html lang="de">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
    <link href="css/download.css" rel="stylesheet">

    <title>Herunterladen</title>

    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
    <link rel="manifest" href="site.webmanifest">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

</head>
<body>
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="main.php"><img style="height: auto;width: 70px;margin-left: 15px" src="img/ui/logo.svg"></a>
    <div style="background-color:#495057;"></div>
    <ul class="navbar-nav px-3 navbar-upload">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="upload.php"><span data-feather="upload-cloud"></span> Hochladen</a>
        </li>
    </ul>
</nav>

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                    <li>
                        <div id="accordion">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <span data-feather="database"></span>
                                            Meine Daten
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="nav-item">
                        <a class="nav-link" href="#">
                            <span data-feather="film"></span>
                            Video
                        </a>
                    </div>
                    <div class="nav-item">
                        <a class="nav-link" href="#">
                            <span data-feather="image"></span>
                            Foto
                        </a>
                    </div>
                    <div class="nav-item">
                        <a class="nav-link" href="#">
                            <span data-feather="mic"></span>
                            Audio
                        </a>
                    </div>
                    <div class="nav-item">
                        <a class="nav-link" href="#">
                            <span data-feather="file"></span>
                            PDF
                        </a>
                    </div>
                                </div>
                            </div>
                            
                        </div>
                </ul>

            </div>
        </nav>

<?php 
//$picturePath = 'media/videos/bigBunny.mp4';
//$picturePath = 'media/bilder/Flower-ICC.jpg';
//$picturePath = 'bilder/1518471954IMG_1181.JPG';
session_start();
if(!isset($_SESSION['userid']))
{
        header("location: login");
}
#LOGOUT
if(isset($_GET['logout'])) {

    session_destroy();
    header('Location: login');
}

$picturePath = $_GET['path'];
include('php/downloadFormular.php');
?>

    </div>

</div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace()
</script>
</body>
</html>
