<?php
session_start();
if(isset($_SESSION['userid']))
{
				header("location: ../main.php");
}
require_once("../db_connect.php");
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// CREATE USER IN FUNCTION
// $statement1 = $pdo->prepare(" INSERT INTO users (passwort,vorname,email) VALUES (:passwort,:vorname,:email)");
// $statement1->bindParam(':passwort', $password);
// $statement1->bindParam(':vorname',$vorname );
// $statement1->bindParam(':email',$email );
//
// $email = "test@test.de";
// $password = password_hash('crossdam', PASSWORD_BCRYPT );
// $vorname = "crossdam";
//
// $pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
// $result1 = $statement1->execute();
// var_dump($result1);




if(isset($_GET['login'])) {
 $vorname = $_POST['vorname'];
 $passwort = $_POST['passwort'];

 $statement = $pdo->prepare("SELECT * FROM users WHERE vorname = :vorname");
 $result = $statement->execute(array('vorname' => $vorname));
 $user = $statement->fetch();


 //Überprüfung des Passworts
 if ($user !== false && password_verify($passwort, $user['passwort'])) {
 $_SESSION['userid'] = $user['id'];
 if(isset($_SESSION['userid']))
 {
 				header("location: ../main.php");
 }
 } else {
 $errorMessage = "Benutzer oder Passwort war ungültig<br>";




 }

}
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
      <link rel="apple-touch-icon" sizes="180x180" href="../apple-touch-icon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="../favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="../favicon-16x16.png">
      <link rel="manifest" href="../site.webmanifest">
      <link rel="mask-icon" href="../safari-pinned-tab.svg" color="#5bbad5">
      <meta name="msapplication-TileColor" content="#da532c">
      <meta name="theme-color" content="#ffffff">

    <title>Anmelden</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="../css/signin.css" rel="stylesheet">
  </head>

  <body class="text-center">
		<div class="container-fluid">
		<div class="row">
			<div class="col">
                <img class="mb-4" src="../favicon.svg" alt="" width="100" height="100">

                <h1>Hive. Alle Daten an einem Platz.</h1><br>

	</div>
	</div>
	<div class="row">

<div class="col">
    <form class="form-signin" action="?login=1" method="post">
        <label for="inputEmail" class="sr-only">Login</label>
      <input type="text" class="form-control" placeholder="Login" name="vorname"  required autofocus>
      <label for="inputPassword" class="sr-only">Passwort</label>
      <input type="password" name="passwort" class="form-control" placeholder="Passwort" required>

      <button style="background-color: rgb(0, 181, 148);border-color: rgb(0, 181, 148);" class="btn btn-lg btn-primary btn-block" type="submit">Anmelden</button>
      <p style="color: #ffffff" class="mt-5 mb-3">&copy; 2018</p>
    </form>
	</div>
	</div>

  </body>
</html>
