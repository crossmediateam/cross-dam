<?php
session_start();
if(!isset($_SESSION['userid']))
{
        header("location: login");
}
#LOGOUT
if(isset($_GET['logout'])) {

    session_destroy();
    header('Location: login');
}

require_once("db_connect.php");0

?>
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Titel</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/add_style.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
<script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="js/jquery.uploadPreview.min.js"></script>
<script type="text/javascript" src="js/fill_input.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
  <div class="container">
<div class="row bg-grey margin-top">
  <div class="col-sm-10">
  </div>
  <div class="col-sm-2 right">
    <form name="logout" action="?logout=1" method="post">
      <input class="btn btn-primary" name="logout" value="logout" type="submit">
</form>
  </div>
</div>
<div class=" row welcome">
  <div class="col-md-12">
  <h1 class="headline">Wilkommen Tom</h1>
</div></div>

<?php



error_reporting(E_ALL);
ini_set('display_errors', 1);
$showBearbeiten=false;
$showUpload=false;





#AUSGABE GESPEICHERTE URL
/*$sql = "SELECT bildurl FROM bildergalerie";
foreach ($pdo->query($sql) as $row) {
   echo $row['bildurl']."<br>";

}
*/




#LÖSCHT BILDER
  if(isset($_GET['bilder'])){
    foreach($_POST['delete'] as $check) {
    $sql ="DELETE FROM bildergalerie WHERE ID ='$check'";
    $pdo->exec($sql);
  }}



#SPEICHERT BILD AUF WEBSPACE

if(isset($_POST["send"]) && $_POST["send"] == "1") {

$target_Path = "bilder/";
$filename = round(microtime(true)).$_FILES["bild_datei"]["name"];
$artist = $_POST["artist"];
$date = $_POST["date"];
$width = $_POST["width"];
$high = $_POST["high"];
$size = $_POST["size"];
$icc = $_POST["icc"];
$marke = $_POST["marke"];
$modell = $_POST["modell"];
$shutter = $_POST["shutter"];
$aperture = $_POST["aperture"];
$iso = $_POST["iso"];
$target_Path = $target_Path . $filename;
move_uploaded_file( $_FILES['bild_datei']['tmp_name'], $target_Path );

#SPEICHERT BILDER IN DB
$sql ="INSERT INTO bildergalerie (bildurl,data_size,width,height,icc,creation_date,artist,make,model,aperture,shutterspeed,iso) VALUES ('$filename','$size','$width','$high','$icc','$date','$artist','$marke','$modell','$aperture','$shutter','$iso')";
$succes=$pdo->exec($sql);
}

echo '<div class="row bg-blue">';

#BUTTON ZEIGT BILDER
if(!$showBearbeiten){
  ?><div class="col-md-6 text-center margin-top">
    <form name="bearbeiten" action="?bearbeiten=1" method="post">
      <input class="btn btn-default action-auswahl" name="bearbeiten" value="Bilder bearbeiten" type="submit">
    </form>
  </div>

  <?php
}

#BUTTON ZEIGT UPLOAD FORM
if(!$showUpload){
  ?><div class="col-md-6 text-center margin-top ">
    <form name="bearbeiten" action="?upload=1" method="post">
      <input class="btn btn-default action-auswahl" style="background-color: rgb(0, 181, 148);border-color: rgb(0, 181, 148);" name="upload" value="Bilder hochladen" type="submit">
    </form>
</div>
  <?php
}
echo '<div class="col-md-12"><hr class="trenner"></div>';





#BEARBEITEN
if(isset($_GET['bearbeiten'])){
  $showBearbeiten=true;
}
if($showBearbeiten){
  echo '<div class="col-md-12">';
echo '<form name="bearbeiten" action="?bilder=1&bearbeiten=1" method="post">';
$sql = "SELECT bildurl, id, art FROM bildergalerie ORDER BY art ASC;";
foreach ($pdo->query($sql) as $row) {
$url=$row['bildurl'];
$id=$row['id'];
$art=$row['art'];
$metatags2 = get_meta_tags("bilder/".$url);
$metatags = exif_read_data("bilder/".$url);

if($art == "1"){
  $art="Damen";
}
elseif($art == "2"){
  $art="Hochsteckfrisuren";
}
elseif($art == "3"){
  $art="Herren";
}

echo '<div class="col-md-3">';
echo '<p>'.$art.'</p>';
  echo'<img alt="Frisur" src="bilder/'.$url.' "><br>';
  highlight_string("<?php\n\$data =\n" . var_export($metatags, true) . ";\n?>");
  highlight_string("<?php\n\$data =\n" . var_export($metatags2, true) . ";\n?>");

echo '<label for="del'.$id.'" class="del-label">';
  echo'<input id="del'.$id.'" type="checkbox" name="delete[]" value="'.$id.'">';
  echo '<span></span></input>
   Löschen</label>';
  echo '</div>';
}
echo '</div>
<div class="col-md-12"><hr class="trenner"></div>
      <div class="row">
      <div class="col-md-12 text-center">
      <input class="btn" type="submit" style="background-color: rgb(0, 181, 148);border-color: rgb(0, 181, 148);" value="Änderungen Speichern">
      </div>
      </div>
      </form>';
}




#UPLOAD
if(isset($_GET['upload'])){
  $showUpload=true;
}
if($showUpload){
  if(!isset($succes)){
    $succes="";}

  ?>

    <div class="col-md-12">
  <form  id="bild_form" class="center upload text-center"enctype="multipart/form-data" action="?speichern=1&upload=1" method="POST" onsubmit="return checkform(this)">
      <!-- MAX_FILE_SIZE muss vor dem Dateiupload Input Feld stehen -->
      <input type="hidden" name="MAX_FILE_SIZE" value="300000000" />
      <input type="hidden" name="send" value="1" />
      <!-- Der Name des Input Felds bestimmt den Namen im $_FILES Array -->
      <input id="image-upload" class="btn center" name="bild_datei" type="file" id="uploadbild" />

<br>


  <div class="center" id="image-preview">
  </div>
  <div class="container_input">
  <div class="row text-left">
    <div class="col-xs-4">
      <label>Fotograf</label><br>
      <input class="meta_input" type="text" name="artist" id="input_artist">
     </div>
    <div class="col-xs-4">
      <label>Aufnahmedatum</label><br>
      <input class="meta_input" type="text" name="date" id="input_date">
    </div>
    <div class="col-xs-4">
    </div>
  </div>
  <div class="row text-left">
    <div class="col-xs-4">
      <label>Dateigröße</label><br>
      <input class="meta_input" type="text" name="size" id="input_size">
     </div>
    <div class="col-xs-4">
      <label>Weite</label><br>
      <input class="meta_input" type="text" name="width" id="input_width">
    </div>
    <div class="col-xs-4">
      <label>Höhe</label><br>
      <input class="meta_input" type="text" name="high" id="input_high">
    </div>
  </div>
  <div class="row text-left">
    <div class="col-xs-4">
      <label>ICC Profil</label><br>
      <input class="meta_input" type="text" name="icc" id="input_icc">
     </div>
    <div class="col-xs-4">
    </div>
    <div class="col-xs-4">
    </div>
  </div>
  <div class="row text-left">
    <div class="col-xs-4">
      <label>Marke</label><br>
      <input class="meta_input" type="text" name="marke" id="input_marke">
     </div>
    <div class="col-xs-4">
      <label>Modell</label><br>
      <input class="meta_input" type="text" name="modell" id="input_modell">
    </div>
    <div class="col-xs-4">
    </div>
  </div>
  <div class="row text-left">
    <div class="col-xs-4">
      <label>Verschlusszeit</label><br>
      <input class="meta_input" type="text" name="shutter" id="input_shutter">
     </div>
    <div class="col-xs-4">
      <label>Blende</label><br>
      <input class="meta_input" type="text" name="aperture" id="input_aperture">
    </div>
    <div class="col-xs-4">
      <label>ISO</label><br>
      <input class="meta_input" type="text" name="iso" id="input_iso">
    </div>
  </div>
</div>
  <div class="row">
  <div class="col-md-12">
  <input  class="btn text-center" type="submit" style="background-color: rgb(0, 181, 148);border-color: rgb(0, 181, 148);" value="hochladen" />
  </div></div>
  <div id="response"></div>
  <?php
  if($succes){
    echo"Bild wurde gespeichert!";
  } ?>
</form>




  <script>
  $(document).ready(function() {
    $.uploadPreview({
      input_field: "#image-upload",
      preview_box: "#image-preview",
      no_label: true
    });
    $('#image-upload').before('<input type="button" id="button-file" class="btn" value="Datei auswählen" />');
$('#image-upload').hide();
$('body').on('click', '#button-file', function() {
    $('#image-upload').trigger('click');
});


  function checkform(form) {
  if( form.getElementById("uploadbild").files.length == 0 ){
      console.log("no files selected");
  }}});



  $(function() {
     $("input:file").change(function (){

// select the file
var fileSelect = document.getElementById('image-upload');
var files = fileSelect;
var myFile = $('#image-upload').prop('files');
var formData = new FormData();

formData.append('bild', files);
formData.append('bild1',myFile[0]);


 // Set up the request.
 var xhr = new XMLHttpRequest();
 xhr.open('POST', 'ajax_controller.php', true);

 // Set up a handler for when the request finishes.
xhr.onload = function () {
  if (xhr.status === 200) {
  var raw_json = xhr.responseText;
  var json = JSON.parse(raw_json);
  fill_input(json);
  console.log(json);
  } else {
    alert('An error occurred!');
  }
};
xhr.send(formData);


     });
  });



  </script>
  <?php
}
?>

</div>
</div></div>
</body>
</html>
